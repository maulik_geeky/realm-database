package com.example.maulik.simplerealm;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.maulik.simplerealm.model.Person;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {
    Button btn_insert, btn_display, btn_delete, btn_JSONData, btn_search;
    EditText edt_name, edt_age, edt_search, edt_id;
    TextView txtData;

    private Realm realm;


    JSONObject jsonObject1, json;
    int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        realm = Realm.getDefaultInstance();

        btn_delete = (Button) findViewById(R.id.btn_delete);

        txtData = (TextView) findViewById(R.id.txt_data);
        btn_insert = (Button) findViewById(R.id.btn_insert);
        edt_name = (EditText) findViewById(R.id.edt_name);
        edt_id = (EditText) findViewById(R.id.edt_id);

        btn_search = (Button) findViewById(R.id.btn_search);
        btn_display = (Button) findViewById(R.id.btn_display);
        btn_JSONData = (Button) findViewById(R.id.btn_JSONData);

        btn_JSONData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMovies();
                //Intent intent = new Intent(MainActivity.this, JSONRealm.class);
                //startActivity(intent);
            }
        });

        btn_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String id = edt_id.getText().toString();
                    String Name = edt_name.getText().toString();

                    if (id.equals("") || Name.equals("")) {
                        Toast.makeText(MainActivity.this, "All field are reuired", Toast.LENGTH_SHORT).show();
                    } else {

                        save_to_database(edt_id.getText().toString().trim(), edt_name.getText().toString().trim());

                    }

                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh_database();
            }
        });
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = edt_id.getText().toString();
                if (id.equals("") || id == null) {
                    edt_id.setHint("Id Require");
                    edt_id.setHintTextColor(Color.RED);
                } else {
                    String name = edt_id.getText().toString().trim();
                    delete_from_database(name);
                }
            }
        });
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RealmSearch.class);
                startActivity(intent);
            }
        });
    }


    private void search_from_database(String name) {
        String SearchResult = "";
        final RealmResults<Person> person = realm.where(Person.class).equalTo("name", name).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

            }
        });
        try {

            int i = 0;
            for (Person search : person) {

                SearchResult += "\t" + person.get(0).getName() + "";
                i++;
            }
            if (SearchResult == "") {
                txtData.setText("No Data Found");
            } else {
                txtData.setText(SearchResult);
            }

            Log.e("Success", "----OK---");
        } catch (Exception e) {
            Log.e("Error : ", e.getMessage());
        }
    }


    private void delete_from_database(String name) {
        try {
            final RealmResults<Person> person = realm.where(Person.class).equalTo("id", name).findAll();
            String res = person.toString();
            if (!res.equals("[]")) {

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        person.deleteFromRealm(0); // Delete and remove object directly

                        Log.e("Success", "----OK---");
                    }
                });
                Toast.makeText(this, "Record Deleted", Toast.LENGTH_SHORT).show();
                refresh_database();
            } else {
                Toast.makeText(this, "No Record Found", Toast.LENGTH_SHORT).show();
                Log.e("Erro :", "No Record Found");
            }
        } catch (Exception e) {
            Log.e("Error Delete : ", e.getMessage());
        }
    }

    private void refresh_database() {
        RealmResults<Person> result = realm.where(Person.class).findAllAsync();
        result.load();
        String output = "";
        String age = "";
        int i = 0;
        for (Person person : result) {


            output += "\t" + result.get(i).getName() + "'s ID is " + result.get(i).getId() + "\n";

            i++;

            //output += person.toString();
        }
        output += "\n";
        txtData.setText(output + " " + age);
    }

    private void save_to_database(final String id, final String name) {


        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                Person user = bgRealm.createObject(Person.class);

                user.setName(name.toUpperCase());
                user.setId(id);

            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                // Transaction was a success.
                Log.e("Success", "----OK-----");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                // Transaction failed and was automatically canceled.
                Log.e("Error : ", error.getMessage());
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void getMovies() {
        try {
            String urlJsonObj = "https://api.myjson.com/bins/148pyj";
            //String urlJsonObj = "http://www.androidbegin.com/tutorial/jsonparsetutorial.txt";
            StringRequest jsonObjReq = new StringRequest(Request.Method.GET,
                    urlJsonObj, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        RealmResults<Person> result2;
                        String result = "";
                        Map<String, String> city = new HashMap<String, String>();
                        //JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = new JSONObject(response);
                        final JSONArray jsonArray = jsonObject.getJSONArray("worldpopulation");
                        for (i = 0; i < jsonArray.length(); i++) {
                            jsonObject1 = jsonArray.getJSONObject(i);

                            //Check if data already in database.
                            result2 = realm.where(Person.class)
                                    .equalTo("id", jsonObject1.getString("id"))
                                    .findAll();

                            result = result2.toString();
                            if (result.equals("[]")) {
                                //If data not available in database thane add to database
                                city.put("id", jsonObject1.getString("id"));
                                city.put("name", jsonObject1.getString("name").toUpperCase());
                                json = new JSONObject(city);
                                loadJsonFromString(json);
                            }
                        }
                        if (result.equals("[]")) {

                        } else {
                            //Display Already data available
                            Log.e("RealmResult", "Already in database");
                        }


                    } catch (JSONException e) {
                        Log.e("Error", e.getMessage());
                        Toast.makeText(MainActivity.this, "Cant Load Data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Error", error.getMessage());
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(jsonObjReq);

            Toast.makeText(this, "Data Loaded", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e("Error : ", e.getMessage());
        }

    }

    private void loadJsonFromString(final JSONObject jsonString) {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.createObjectFromJson(Person.class, jsonString.toString());
                Log.e("JsonString : ", jsonString.toString());
            }
        });
    }

}
