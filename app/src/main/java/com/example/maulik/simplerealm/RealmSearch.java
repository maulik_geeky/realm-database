package com.example.maulik.simplerealm;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maulik.simplerealm.model.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

import static android.R.attr.editable;

public class RealmSearch extends AppCompatActivity {
    String[] language = {""};
    Realm realm;

    TextView txt_searched_item;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realm_search);

        realm = Realm.getDefaultInstance();
        //Creating the instance of ArrayAdapter containing list of language names


        txt_searched_item = (TextView) findViewById(R.id.txt_searched_item);
        adapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, language);

        //Getting the instance of AutoCompleteTextView
        final AutoCompleteTextView actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);
        actv.setThreshold(1);//will start working from first character
        actv.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
        actv.setTextColor(Color.RED);
        actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(RealmSearch.this, "Selected item  is : " + actv.getText().toString(), Toast.LENGTH_SHORT).show();
                txt_searched_item.setText("Your Country is : " + actv.getText().toString());
            }
        });

        actv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.e("Text is : ", charSequence.toString());
                String SearchResult = "";
                final RealmResults<Person> person = realm.where(Person.class).contains("name", charSequence.toString().toUpperCase()).findAll();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                    }
                });
                try {
                    int x = 0;
                    HashSet<String> hashSet = new HashSet<String>();
                    if (person.size() != 0) {
                        for (Person per : person) {
                            hashSet.add(person.get(x).getName());
                            x++;
                        }
                        Log.e("Empty : ", "Added to hashset");
                    } else {
                        Log.e("Empty : ", "person is empty");
                    }
                    adapter.clear();
                    adapter.addAll(hashSet);

                    Log.e("Success", "----OK---");
                } catch (Exception e) {
                    Log.e("Error : ", "Invalid data");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }
}
